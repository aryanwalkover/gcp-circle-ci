FROM node:14

# A directory within the virtualized Docker environment
# Becomes more relevant when using Docker Compose later
RUN mkdir -p /gcp-circleci
WORKDIR /gcp-circleci

# Copies package.json and package-lock.json to Docker environment
#COPY package*.json ./

# Installs all node packages
#RUN npm install

# Copies everything over to Docker environment
COPY . /gcp-circleci/

# Uses port which is used by the actual application
EXPOSE 9090
RUN npm install

RUN npm install -g serve

RUN yarn build

CMD serve -s build -p 9090